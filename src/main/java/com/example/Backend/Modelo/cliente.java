package com.example.Backend.Modelo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TemporalType;
import javax.persistence.Temporal;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
//nombre de entidad "cliente"
@Table(name="cliente") 
public class cliente {
    //atributos
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_cliente;

   
    @Column(nullable = false,length = 10,unique = true)
    private String cedula;

    @Column(nullable = false,length = 40)
    private String nombre;
    @Column(nullable = false,length = 40)
    private String direccion;
    @Column(nullable = false,length = 40)
    private Long telefono;
     @Column(nullable = false)
    private Boolean eliminado=true ;

     @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_creacion")
     @DateTimeFormat(pattern = "YYYY-MM-DDThh:mm:ssTZD")
    private Date fecha_creacion;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_modificacion")
    @DateTimeFormat(pattern = "YYYY-MM-DDThh:mm:ssTZD")
    private Date fecha_modificacion;




    public cliente(Long id_cliente, String cedula, String nombre, String direccion, Long telefono, Boolean eliminado, Date fecha_creacion, Date fecha_modificacion) {
        this.id_cliente = id_cliente;
        this.cedula = cedula;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.eliminado = eliminado;
        this.fecha_creacion = fecha_creacion;
        this.fecha_modificacion = fecha_modificacion;
    }
  

    public Long getId_cliente() {
        return this.id_cliente;
    }

    public void setId_cliente(Long id_cliente) {
        this.id_cliente = id_cliente;
    }

    public cliente() {
    }
   


    public String getCedula() {
        return this.cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Long getTelefono() {
        return this.telefono;
    }

    public void setTelefono(Long telefono) {
        this.telefono = telefono;
    }

    public Boolean isEliminado() {
        return this.eliminado;
    }

    public Boolean getEliminado() {
        return this.eliminado;
    }

    public void setEliminado(Boolean eliminado) {
        this.eliminado = eliminado;
    }

    public Date getFecha_creacion() {
        return this.fecha_creacion;
    }

    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public Date getFecha_modificacion() {
        return this.fecha_modificacion;
    }

    public void setFecha_modificacion(Date fecha_modificacion) {
        this.fecha_modificacion = fecha_modificacion;
    }
  
    
}
