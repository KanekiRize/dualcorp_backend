package com.example.Backend.Servicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.Backend.Modelo.cliente;
import com.example.Backend.Repositorio.clienteRepositorio;


@Service
public class clienteServicio {

   @Autowired
    private clienteRepositorio rep;

    public List<cliente> listar() {
        return this.rep.findAll();
    }
    // Metodo para crear un nuevo cliente
    public cliente crearCliente(cliente cliente ) {
        if(rep.findByCedula(cliente.getCedula())!= null){
            return new cliente();
        }
        return this.rep.save(cliente);
    }
    // Metodo para eliminar al cliente por su cedula de manera fisica
    public void eliminarCliente(String cedula) {
        if(rep.findByCedula(cedula)!= null){
            rep.deleteById(cedula);
        }
    }

    // Metodo modificar los datos del cliente
    public cliente editarCliente(String cedula, cliente cliente) {
        if (rep.findByCedula(cedula)!= null) {
            cliente clienteUpdate=rep.findByCedula(cedula);
            clienteUpdate = cliente;
            return rep.save(clienteUpdate);		
        }
       return cliente;
    }
    //buscar por cedula del cliente
    public cliente buscarPorCedula(String cedula){
        return rep.findByCedula(cedula);
    }
         
    public void modificarEstado( String cedula){
        this.rep.modificarEstado( cedula);
     }
 
     public List<cliente> listatrue() {
        return rep.listaExistente();
    }
    
}
