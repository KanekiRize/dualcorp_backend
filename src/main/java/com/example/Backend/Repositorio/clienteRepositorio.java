package com.example.Backend.Repositorio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.Backend.Modelo.cliente;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
@Repository
public interface clienteRepositorio extends JpaRepository<cliente, String>  {
    public cliente findByCedula(String  Cedula);

    @Modifying
    @Transactional
    @Query(nativeQuery = true,value = "UPDATE cliente set  eliminado= false  WHERE cedula =?1 ")
    void modificarEstado(String cedula);

    //Para visualizar los que aun no se han eliminado
    @Query(value = "SELECT * FROM cliente  WHERE  eliminado= true", nativeQuery = true)
    List<cliente> listaExistente();

    
    
}
