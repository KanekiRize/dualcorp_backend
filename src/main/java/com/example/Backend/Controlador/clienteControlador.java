package com.example.Backend.Controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Backend.Modelo.cliente;
import com.example.Backend.Servicio.clienteServicio;

@RestController
@RequestMapping("/cliente")
@CrossOrigin(origins = "*")
public class clienteControlador {
     @Autowired
    private clienteServicio servicio;
    
    //listar todos los clientes registrados
    @GetMapping("/listar")
    public List<cliente> listar() {
        return this.servicio.listar();

    }
  //lista los clientes que aun no se han eliminado
    @GetMapping("/listatrue")
    @CrossOrigin
    public List<cliente> listarTrue() {
        return this.servicio.listatrue();
    }

    //guardar los clientes
     @PostMapping("/guardar")
     @CrossOrigin
    public cliente crearCliente(@RequestBody cliente cliente ) {
        return this.servicio.crearCliente(cliente);
    }

     //eliminar los clientes de manera fisica
    @DeleteMapping("/eliminar/{cedula}")
    @CrossOrigin
    public void eliminarCliente(@PathVariable String cedula) {
        this.servicio.eliminarCliente(cedula);
    }
    //buscar por cedula al clientes
     @GetMapping("/buscarporcedula/{cedula}")
     public cliente buscarPorCedulas(@PathVariable String cedula){
        return servicio.buscarPorCedula(cedula);
     } 

    //Modificar los clientes
    @PutMapping("/editar/{cedula}")
    @CrossOrigin
    public cliente editarCliente(@PathVariable String cedula, @RequestBody cliente cliente) {
       return this.servicio.editarCliente(cedula, cliente);
    }

    //modificar el estado
    @PutMapping("/eliminar/{cedula}")
    @CrossOrigin
    public void modificar_estado(@PathVariable String cedula) {
        this.servicio.modificarEstado(cedula);
    }

}
